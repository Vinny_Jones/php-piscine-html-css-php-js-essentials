<?PHP
	include 'auth.php';
	session_start();
	$_SESSION['loggued_on_user'] = "";
	if (auth($_POST['login'], $_POST['passwd']))
		$_SESSION['loggued_on_user'] = $_POST['login'];
	else
	{
		echo "ERROR\n";
		header('Location: index.html');
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Chat</title>
		<style>
			.maindiv {
				position: absolute;
				width: 850px;
				height: 800px;
				background-color: #EFD5F2;
				font-weight: bold;
				font-size:30px;
				color: #864C9F;
				text-align: center;
				margin-left: 25%;
				margin-top: 50px;
				border-radius: 40px;
			}
			a:hover {
				background-color: #864C9F;
				color: white;
			}
			button:hover {
				background-color: #864C9F;
				color: white;
			}
		</style>
	</head>
	<body style="background-image: url('http://www.allmacwallpaper.com/get/iMac-27-inch-wallpapers/Landscape-42-2560x1440/364-10.jpg'); height: 100%;">
		<div class="maindiv">
			<div style="font-family: 'Raleway', Arial, sans-serif; font-size:40px; width: 100%; float: left; margin-top: 5px;">User: <?PHP echo $_SESSION['loggued_on_user'] ?></div>

			<div style="position: relative; width: 30%; float: left; height: 20px;"></div>
			<div style="position: relative; width: 40%; float: left; border: 0px; font-size: 20px; margin-top: 10px;">
				<a style="display:block;" href="logout.php">LOG OUT</a>
			</div>
			<div style="float: left: text-align:center; width: 600px; margin-left: 150px;">
				<iframe name="chat" src="chat.php" width="100%" height="550px" style="border: solid 2px; margin-top: 25px;"></iframe>
				<iframe name="speak" src="speak.php" width="100%" height="50px" style="border: solid 2px;"></iframe>
			</div>
		</div>
	</body>
</html>

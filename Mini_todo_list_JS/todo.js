function delete_item() {
	if (confirm("Are you sure?"))
	{
		document.cookie = this.id + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
		document.getElementById(this.id).remove();
	}
}

function delete_all() {
	var all = document.cookie.split(';');
	if (confirm("Are you sure?") && all[0] != "")
	{
		for (var i = 0; i < all.length ; i++) {
			var temp = all[i].split('=');
			document.getElementById(temp[0].trim()).remove();
			document.cookie = temp[0].trim() + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
		}
	}
}

function fill_items() {
	var all = document.cookie.split(';');
	if (all[0] != "")
		for (var i = 0; i < all.length ; i++) {
			var temp = all[i].split('=');
			item_add(temp[0].trim(), decodeURIComponent(temp[1]));
		}
}

function create_item() {
	var item_data = prompt("Please fill new TO-DO item", "");
	var date_id = new Date();
	var div_id = date_id.getTime();

	if (item_data != null && empty_check(item_data))
	{
		item_add(div_id, item_data);
		date_id.setTime(date_id.getTime() + 30 * 24 * 60 * 60 * 1000);
		document.cookie = div_id + "=" + encodeURIComponent(item_data) + ";" + "expires=" + date_id.toUTCString();
	}
}


function item_add(id, value) {
	var new_div = document.createElement('div');
	new_div.className = "list_item";
	new_div.id = id;
	new_div.textContent = value;
	new_div.addEventListener("click", delete_item);

	var list = document.getElementById("ft_list");
	list.insertBefore(new_div, list.childNodes[0]);
}

function empty_check(str)
{
	if (str == "")
	{
		alert("Item must contain something\nTry again");
		return (0);
	}
	return (1);
}

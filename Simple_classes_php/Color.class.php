<?php
class Color {

	static $verbose = False;
	public $red = 0;
	public $green = 0;
	public $blue = 0;

	static function doc() {
		return (file_get_contents("Color.doc.txt"));
	}

	public function add(Color $added) {
		$new_red = $this->red + $added->red;
		$new_green = $this->green + $added->green;
		$new_blue = $this->blue + $added->blue;
		return (new Color (array('red' => $new_red, 'green' => $new_green, 'blue' => $new_blue)));
	}

	public function sub(Color $substr) {
		$new_red = $this->red - $substr->red;
		$new_green = $this->green - $substr->green;
		$new_blue = $this->blue - $substr->blue;
		return (new Color (array('red' => $new_red, 'green' => $new_green, 'blue' => $new_blue)));
	}

	public function mult($k) {
		return (new Color (array('red' => $this->red * $k, 'green' => $this->green * $k, 'blue' => $this->blue * $k)));
	}

	public function __toString() {
		return (sprintf("Color( red: %3d, green: %3d, blue: %3d )", $this->red, $this->green, $this->blue));
	}

	function __construct(array $input) {
		if (isset($input['rgb'])) {
			$color = intval($input['rgb']);
			$this->red = ($color >> 16) & 0xFF;
			$this->green = ($color >> 8) & 0xFF;
			$this->blue = $color & 0xFF;
		}
		else {
			$this->red = intval($input['red']);
			$this->green = intval($input['green']);
			$this->blue = intval($input['blue']);
		}
		if (self::$verbose)
			print($this->__toString()." constructed.\n");
		return ;
	}
	function __destruct() {
		if (self::$verbose)
			print($this->__toString()." destructed.\n");
		return ;
	}
}
?>

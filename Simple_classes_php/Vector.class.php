<?php

require_once 'Vertex.class.php';

class Vector {

	static		$verbose = False;
	private		$_x = 0;
	private		$_y = 0;
	private		$_z = 0;
	private		$_w = 0;

	public function get_X() {
		return ($this->_x);
	}

	public function get_Y() {
		return ($this->_y);
	}

	public function get_Z() {
		return ($this->_z);
	}

	public function get_W() {
		return ($this->_w);
	}


	public function cos(Vector $rhs) {
		return ($this->dotProduct($rhs) / ($this->magnitude() * $rhs->magnitude()));
	}

	public function crossProduct(Vector $rhs) {
		$new_x = $this->_y * $rhs->_z - $this->_z * $rhs->_y;
		$new_y = $this->_z * $rhs->_x - $this->_x * $rhs->_z;
		$new_z = $this->_x * $rhs->_y - $this->_y * $rhs->_x;
		return (new Vector(array('dest' => new Vertex(array('x' => $new_x,
			'y' => $new_y, 'z' => $new_z)))));
	}

	public function dotProduct(Vector $rhs) {
		return ($this->_x * $rhs->_x + $this->_y * $rhs->_y + $this->_z * $rhs->_z);
	}

	public function scalarProduct($k) {
		return (new Vector(array('dest' => new Vertex(array('x' => $this->_x * $k,
			'y' => $this->_y * $k, 'z' => $this->_z * $k)))));
	}

	public function opposite() {
		return (new Vector(array('dest' => new Vertex(array('x' => $this->_x * -1,
			'y' => $this->_y * -1, 'z' => $this->_z * -1)))));
	}

	public function sub(Vector $rhs) {
		return (new Vector(array('dest' => new Vertex(array('x' => $this->_x - $rhs->_x,
			'y' => $this->_y - $rhs->_y, 'z' => $this->_z - $rhs->_z)))));
	}

	public function add(Vector $rhs) {
		return (new Vector(array('dest' => new Vertex(array('x' => $this->_x + $rhs->_x,
			'y' => $this->_y + $rhs->_y, 'z' => $this->_z + $rhs->_z)))));
	}

	public function normalize() {
		$mag = $this->magnitude();
		$dest = new Vertex(array('x' => $this->_x / $mag, 'y' => $this->_y / $mag, 'z' => $this->_z / $mag));
		return (new Vector(array('dest' => $dest)));
	}

	public function magnitude() {
		return (sqrt($this->_x * $this->_x + $this->_y * $this->_y + $this->_z * $this->_z));
	}

	public function __toString() {
		return (sprintf("Vector( x:%.2f, y:%.2f, z:%.2f, w:%.2f )", $this->_x, $this->_y, $this->_z, $this->_w));
	}

	static function doc() {
		return (file_get_contents("Vector.doc.txt"));
	}

	function __construct(array $input) {
		if(isset($input['dest']) && ($input['dest'] INSTANCEOF Vertex))
		{
			$dest = $input['dest'];
			if (isset($input['orig']) && ($input['orig'] INSTANCEOF Vertex))
				$orig = $input['orig'];
			else
				$orig = new Vertex(array('x' => 0, 'y' => 0, 'z' => 0));
			$this->_x = $dest->get_X() - $orig->get_X();
			$this->_y = $dest->get_Y() - $orig->get_Y();
			$this->_z = $dest->get_Z() - $orig->get_Z();
			$this->_w = $dest->get_W() - $orig->get_W();
		}
		if (self::$verbose)
			print($this->__toString()." constructed\n");
		return ;
	}
	function __destruct() {
		if (self::$verbose)
			print($this->__toString()." destructed\n");
		return ;
	}
}
?>

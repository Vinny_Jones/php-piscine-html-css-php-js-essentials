<?php

require_once 'Color.class.php';

class Vertex {

	static		$verbose = False;
	private		$_x = 0;
	private		$_y = 0;
	private		$_z = 0;
	private		$_w = 1.0;
	private		$_color;

	public function get_X() {
		return ($this->_x);
	}

	public function get_Y() {
		return ($this->_y);
	}

	public function get_Z() {
		return ($this->_z);
	}

	public function get_W() {
		return ($this->_w);
	}

	public function get_CLR() {
		return ($this->_color);
	}

	public function set_X($x) {
		$this->_x = floatval($x);
	}

	public function set_Y($y) {
		$this->_y = floatval($y);
	}

	public function set_Z($z) {
		$this->_z = floatval($z);
	}

	public function set_W($w) {
		$this->_w = floatval($w);
	}

	public function set_CLR(Color $clr) {
		$this->_color = $clr;
	}

	public function __toString() {
		if (self::$verbose)
			return (sprintf("Vertex( x: %.2f, y: %.2f, z:%.2f, w:%.2f, %s )", $this->_x, $this->_y,
				$this->_z, $this->_w, (string)$this->_color));
		return (sprintf("Vertex( x: %.2f, y: %.2f, z:%.2f, w:%.2f )", $this->_x, $this->_y,
			$this->_z, $this->_w));
	}

	static function doc() {
		return (file_get_contents("Vertex.doc.txt"));
	}

	function __construct(array $input) {
		if(isset($input['x']) && isset($input['y']) && isset($input['z']))
		{
			$this->_x = floatval($input['x']);
			$this->_y = floatval($input['y']);
			$this->_z = floatval($input['z']);
			if (isset($input['w']))
				$this->_w = floatval($input['w']);
			if (isset($input['color']) && ($input['color'] INSTANCEOF Color))
				$this->_color = $input['color'];
			else
				$this->_color = new Color(array('red' => 255, 'green' => 255, 'blue' => 255));
		}
		if (self::$verbose)
			print($this->__toString()." constructed\n");
		return ;
	}
	function __destruct() {
		if (self::$verbose)
			print($this->__toString()." destructed\n");
		return ;
	}
}
?>

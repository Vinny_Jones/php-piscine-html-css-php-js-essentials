<?php

require_once 'Vertex.class.php';
require_once 'Vector.class.php';
require_once 'Matrix.class.php';

class Matrix
{
	protected $main_mtx = array();
	private $_key_preset;
	private $_key_scale;
	private $_key_angle;
	private $_key_vtc;
	private $_key_fov;
	private $_key_ratio;
	private $_key_near;
	private $_key_far;
	static $verbose = false;
	const IDENTITY = "IDENTITY";
	const SCALE = "SCALE";
	const RX = "Ox ROTATION";
	const RY = "Oy ROTATION";
	const RZ = "Oz ROTATION";
	const TRANSLATION = "TRANSLATION";
	const PROJECTION = "PROJECTION";

	public static function doc()
	{
		return (file_get_contents("Matrix.doc.txt"));
	}
	
	function __toString()
	{
		$str = "M | vtcX | vtcY | vtcZ | vtxO\n";
		$str .= "-----------------------------\n";
		$str .= "x | %0.2f | %0.2f | %0.2f | %0.2f\n";
		$str .= "y | %0.2f | %0.2f | %0.2f | %0.2f\n";
		$str .= "z | %0.2f | %0.2f | %0.2f | %0.2f\n";
		$str .= "w | %0.2f | %0.2f | %0.2f | %0.2f";
		return (vsprintf($str, array($this->matrix[0], $this->matrix[1], $this->matrix[2], $this->matrix[3], $this->matrix[4], $this->matrix[5], $this->matrix[6], $this->matrix[7], $this->matrix[8], $this->matrix[9], $this->matrix[10], $this->matrix[11], $this->matrix[12], $this->matrix[13], $this->matrix[14], $this->matrix[15])));
	}

	public function __construct($arr = null)
	{
		if (isset($arr)) {
			if (isset($arr['preset']))
				$this->_preset = $arr['preset'];
			if (isset($arr['scale']))
				$this->_scale = $arr['scale'];
			if (isset($arr['angle']))
				$this->_angle = $arr['angle'];
			if (isset($arr['vtc']))
				$this->_vtc = $arr['vtc'];
			if (isset($arr['fov']))
				$this->_fov = $arr['fov'];
			if (isset($arr['ratio']))
				$this->_ratio = $arr['ratio'];
			if (isset($arr['near']))
				$this->_near = $arr['near'];
			if (isset($arr['far']))
				$this->_far = $arr['far'];
			$this->check();
			$this->makemtx();
			if (self::$verbose) {
				if ($this->_preset == self::IDENTITY)
					echo "Matrix " . $this->_preset . " instance constructed\n";
				else
					echo "Matrix " . $this->_preset . " preset instance constructed\n";
			}
			$this->disp();
		}
	}

	function __destruct()
	{
		if (self::$verbose) {
			printf("Matrix instance destructed\n");
		}
	}

	private function disp()
	{
		switch ($this->_preset) {
			case (self::IDENTITY) :
				$this->idt(1);
				break;
			case (self::TRANSLATION) :
				$this->trans();
				break;
			case (self::SCALE) :
				$this->idt($this->_scale);
				break;
			case (self::RX) :
				$this->rot_x();
				break;
			case (self::RY) :
				$this->rot_y();
				break;
			case (self::RZ) :
				$this->rot_z();
				break;
			case (self::PROJECTION) :
				$this->projection();
				break;
		}
	}

	private function makemtx()
	{
		for ($i = 0; $i < 16; $i++) {
			$this->matrix[$i] = 0;
		}
	}

	private function idt($scale)
	{
		$this->matrix[0] = $scale;
		$this->matrix[5] = $scale;
		$this->matrix[10] = $scale;
		$this->matrix[15] = 1;
	}

	private function trans()
	{
		$this->idt(1);
		$this->matrix[3] = $this->_vtc->get_X();
		$this->matrix[7] = $this->_vtc->get_Y();
		$this->matrix[11] = $this->_vtc->get_Z();
	}

	private function rot_x()
	{
		$this->idt(1);
		$this->matrix[0] = 1;
		$this->matrix[5] = cos($this->_angle);
		$this->matrix[6] = -sin($this->_angle);
		$this->matrix[9] = sin($this->_angle);
		$this->matrix[10] = cos($this->_angle);
	}

	private function rot_y()
	{
		$this->idt(1);
		$this->matrix[0] = cos($this->_angle);
		$this->matrix[2] = sin($this->_angle);
		$this->matrix[5] = 1;
		$this->matrix[8] = -sin($this->_angle);
		$this->matrix[10] = cos($this->_angle);
	}

	private function rot_z()
	{
		$this->idt(1);
		$this->matrix[0] = cos($this->_angle);
		$this->matrix[1] = -sin($this->_angle);
		$this->matrix[4] = sin($this->_angle);
		$this->matrix[5] = cos($this->_angle);
		$this->matrix[10] = 1;
	}
	private function projection()
	{
		$this->idt(1);
		$this->matrix[5] = 1 / tan(0.5 * deg2rad($this->_fov));
		$this->matrix[0] = $this->matrix[5] / $this->_ratio;
		$this->matrix[10] = -1 * (-$this->_near - $this->_far) / ($this->_near - $this->_far);
		$this->matrix[14] = -1;
		$this->matrix[11] = (2 * $this->_near * $this->_far) / ($this->_near - $this->_far);
		$this->matrix[15] = 0;
	}

	private function check()
	{
		if (empty($this->_preset))
			return "error";
		if ($this->_preset == self::SCALE && empty($this->_scale))
			return "error";
		if (($this->_preset == self::RX || $this->_preset == self::RY || $this->_preset == self::RZ) && empty($this->_angle))
			return "error";
		if ($this->_preset == self::TRANSLATION && empty($this->_vtc))
			return "error";
		if ($this->_preset == self::PROJECTION && (empty($this->_fov) || empty($this->_radio) || empty($this->_near) || empty($this->_far)))
			return "error";
	}

	public function mult(Matrix $rhs)
	{
		$tmp = array();
		for ($i = 0; $i < 16; $i += 4) {
			for ($j = 0; $j < 4; $j++) {
				$tmp[$i + $j] = 0;
				$tmp[$i + $j] += $this->matrix[$i + 0] * $rhs->matrix[$j + 0];
				$tmp[$i + $j] += $this->matrix[$i + 1] * $rhs->matrix[$j + 4];
				$tmp[$i + $j] += $this->matrix[$i + 2] * $rhs->matrix[$j + 8];
				$tmp[$i + $j] += $this->matrix[$i + 3] * $rhs->matrix[$j + 12];
			}
		}
		$matrice = new Matrix();
		$matrice->matrix = $tmp;
		return $matrice;
	}

	public function transformVertex(Vertex $vtx)
	{
		$tmp = array();
		$tmp['x'] = ($vtx->get_x() * $this->matrix[0]) + ($vtx->get_y() * $this->matrix[1]) + ($vtx->get_z() * $this->matrix[2]) + ($vtx->get_w() * $this->matrix[3]);
		$tmp['y'] = ($vtx->get_x() * $this->matrix[4]) + ($vtx->get_y() * $this->matrix[5]) + ($vtx->get_z() * $this->matrix[6]) + ($vtx->get_w() * $this->matrix[7]);
		$tmp['z'] = ($vtx->get_x() * $this->matrix[8]) + ($vtx->get_y() * $this->matrix[9]) + ($vtx->get_z() * $this->matrix[10]) + ($vtx->get_w() * $this->matrix[11]);
		$tmp['w'] = ($vtx->get_x() * $this->matrix[11]) + ($vtx->get_y() * $this->matrix[13]) + ($vtx->get_z() * $this->matrix[14]) + ($vtx->get_w() * $this->matrix[15]);
		$tmp['color'] = $vtx->get_CLR();
		$vertex = new Vertex($tmp);
		return $vertex;
	}
}
?>

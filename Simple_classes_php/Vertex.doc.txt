<- Vertex ----------------------------------------------------------------------
Class represents a vertex according to five characteristics.
The Class’ constructor is waiting for an array. The following keys are required:
• "x" axis coordinate (mandatory);
• "y" axis coordinate (mandatory);
• "z" depth coordinate (mandatory);
• "w" homogeneous coordinate (optional);
• "color" represented by an instance of the Color Class (optional).

Reading/writing functions:
get_X();
get_Y();
get_Z();
get_W();
get_CLR() - get Color instance;
set_X();
set_Y();
set_Z();
set_W();
set_CLR() - set Color instance;
---------------------------------------------------------------------- Vertex ->

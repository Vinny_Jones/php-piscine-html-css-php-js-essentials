$(document).ready(function(){
	fill_items();
	$("#new_button").click(function () {
		var item_data = prompt("Please fill new TO-DO item", "");

		if (item_data != null && item_data != "")
		{
			id = $.now();
			$("#ft_list").prepend($("<div class=\"list_item\" id=\"" + id + "\">" + item_data + "</div>").click(delete_item));
			var date_id = new Date(id + 30 * 24 * 60 * 60 * 1000);
			document.cookie = id + "=" + encodeURIComponent(item_data) + ";" + "expires=" + date_id.toUTCString();
		}
		else if (item_data == "")
			alert("Item must contain something\nTry again");
	});

	$("#clear_button").click(function() {
		var all = document.cookie.split(';');
		if (confirm("Are you sure?") && all[0] != "")
		{
			$("#ft_list").empty();
			for (var i = 0; i < all.length ; i++) {
				var temp = all[i].split('=');
				document.cookie = temp[0] + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
			}
		}
	});

});

function delete_item() {
	if (confirm("Are you sure?"))
	{
		document.cookie = this.id + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
		this.remove();
	}
}

function fill_items() {
	var all = document.cookie.split(';');
	if (all[0] != "")
		for (var i = 0; i < all.length ; i++) {
			var temp = all[i].split('=');
			var id = temp[0].trim();
			var item_data = decodeURIComponent(temp[1]);
			$("#ft_list").prepend($("<div class=\"list_item\" id=\"" + id + "\">" + item_data + "</div>").click(delete_item));
		}
}

<?php
	if (file_exists("list.csv"))
	{
		$fd = fopen("list.csv", "r");
		flock($fd, LOCK_EX);
		$input = trim(file_get_contents("list.csv"));
		if (!empty($input))
			$arr = explode(",", $input);
		foreach ($arr as $key => $value)
			$arr[$key] = trim($value);

		$arr = array_filter($arr);
		echo json_encode($arr);
		flock($fd, LOCK_UN);
		fclose($fd);
	}
?>

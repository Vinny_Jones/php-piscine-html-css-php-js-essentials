$(document).ready(function(){

	$.ajax({
       url: 'select.php',
       success: function (response) {
           var arr = JSON.parse(response);
           if (Array.isArray(arr) && arr[0] != '') {
               for (i = 0; i < arr.length; i++) {
                   if (arr[i] != '') {
                       tmp = arr[i].split(';');
					   $("#ft_list").prepend($("<div class=\"list_item\" id=\"" + tmp[0] + "\">" + decodeURIComponent(tmp[1]) + "</div>").click(delete_item));
                   }
               }
           }
       }
       });

	$("#new_button").click(function(){
		var item_data = prompt("Please fill new TO-DO item", "");

		if (item_data != null && item_data != "")
		{
			id = $.now();
			$("#ft_list").prepend($("<div class=\"list_item\" id=\"" + id + "\">" + item_data + "</div>").click(delete_item));

			$.ajax({
				 url:"insert.php",
				 method:"POST",
				 data:
				 {
					  list_id:id,
					  list_value:encodeURIComponent(item_data)
				 }
			 });

		}
		else if (item_data == "")
			alert("Item must contain something\nTry again");
	});

	$("#clear_button").click(function(){
		if (confirm("Are you sure?"))
		{
			$("#ft_list").empty();
			$.ajax({
				 url:"delete.php",
				 method:"POST",
				 data:
				 {
					  list_id:"all",
				 }
			 });

		}
	});

	function delete_item() {
		if (confirm("Are you sure?"))
		{
			this.remove();
			$.ajax({
				 url:"delete.php",
				 method:"POST",
				 data:
				 {
					  list_id:this.id
				 }
			 });
		}
	}

});

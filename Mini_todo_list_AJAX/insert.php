<?php
	if ($_POST['list_id'] && $_POST['list_value'] && file_exists("list.csv"))
	{
		$fd = fopen("list.csv", "r+");
		flock($fd, LOCK_EX);
		$input = trim(file_get_contents("list.csv"));
		if (!empty($input))
			$arr = explode(",", $input);
		$arr = array_filter($arr);

		foreach ($arr as $key => $value)
			$arr[$key] = trim($value).",\n";
		$arr[] = $_POST['list_id'].";".$_POST['list_value'].",";
		file_put_contents("list.csv", implode($arr));
		flock($fd, LOCK_UN);
		fclose($fd);
	}
?>

<?php
	if ($_POST['list_id'] == "all")
		file_put_contents("list.csv", "");
	else if ($_POST['list_id'])
	{
		$fd = fopen("list.csv", "r+");
		flock($fd, LOCK_EX);
		$input = trim(file_get_contents("list.csv"));
		if (!empty($input))
			$arr = explode(",", $input);
		$arr = array_filter($arr);

		foreach ($arr as $key => $value)
		{
			$temp = explode(";", trim($value));
			if ($temp[0] == $_POST['list_id'])
			{
				$arr[$key] = "";
			}
			else
				$arr[$key] = trim($arr[$key]).",\n";

		}
		$arr = array_filter($arr);
		file_put_contents("list.csv", implode($arr));
		flock($fd, LOCK_UN);
		fclose($fd);
	}
?>

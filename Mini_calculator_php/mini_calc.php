#!/usr/bin/php
<?PHP
	if ($argc == 2)
	{
		function errorexit()
		{
			echo ("Syntax Error\n");
			exit(0);
		}
		$str_sub = trim($argv[1]);
		$num1 = floatval($str_sub);
		if ($str_sub[0] == '+')
			$str_sub = substr($str_sub, 1);
		if (strcmp(strval($num1), substr($str_sub, 0, strlen($num1))))
			errorexit();
		$str_sub = trim(substr($str_sub, strlen($num1)));
		$oper = $str_sub[0];
		$str_sub = trim(substr($str_sub, 1));
		$num2 = floatval($str_sub);
		if ($str_sub[0] == '+')
			$str_sub = substr($str_sub, 1);
		if (strcmp(strval($num2), $str_sub))
			errorexit();
		if ($oper == '-')
			echo $num1 - $num2."\n";
		else if ($oper == '+')
			echo $num1 + $num2."\n";
		else if ($oper == '*')
			echo $num1 * $num2."\n";
		else if ($oper == '/' && $num2)
			echo $num1 / $num2."\n";
		else if ($oper == '%' && $num2)
			echo $num1 % $num2."\n";
		else
			echo ("Syntax Error\n");
	}
	else
		echo "Incorrect Parameters\n";
?>
